<%@ include file="/WEB-INF/view/module/legacyui/template/include.jsp" %>

<openmrs:message var="pageTitle" code="help.title" scope="page"/>
<%@ include file="/WEB-INF/view/module/legacyui/template/header.jsp" %>

<h2><openmrs:message code="help.title"/></h2>

<br />
<openmrs:message code="help.wiki.text" htmlEscape="false" arguments="<a href='https://wiki.openmrs.org/x/GAAJ'>,</a>"/>
<br /><br/>
<ul>
	<li><h4><openmrs:message code="help.overview.text" /></h4></li>
	<br/>
	<ul>
	<li><openmrs:message code="help.about.text" htmlEscape="false" arguments="<a href='https://librehealth.gitbooks.io/lh-toolkit-dev-guide/content/high_level_introduction.html'>,</a>"/></li>
	<br/>
	<li><openmrs:message code="help.technical.text" htmlEscape="false" arguments="<a href='https://librehealth.gitbooks.io/lh-toolkit-dev-guide/content/technical_overview.html'>,</a>"/></li>
	<br/>
	</ul>
	<li><h4><openmrs:message code="help.guides.text"/></h4></li>
	<br/>
	<ul>
	<li><a href="https://librehealth.gitbooks.io/lh-toolkit-dev-guide/content/librehealth-information-model.html"><openmrs:message code="help.getting.started.guide.text"/></a></li>
	<br/>
	<li><a href=""><openmrs:message code="help.user.text"/></a></li>
	<br/>
	<li><a href="https://librehealth.gitbooks.io/lh-toolkit-dev-guide/content/librehealth_toolkit_installation.html"><openmrs:message code="help.developer.text"/></a></li>
	<br/>
	<li><a href=""><openmrs:message code="help.administrator.text"/></a></li>
	<br/>
	<li><a href=""><openmrs:message code="help.modules.text"/></a></li>
	<br/>
	<li><a href=""><openmrs:message code="help.troubleshoot.text"/></a></li>
	</ul>
</ul>
<br />
<openmrs:message code="help.contact.text"/>
<br />
<ul>
	<li><openmrs:message code="help.irc.text" htmlEscape="false" arguments="<a href=''>,</a>"/></li>
	<br>
	<li><openmrs:message code="help.mailing.text" htmlEscape="false" arguments="<a href=''>,</a>"/></li>
</ul>


<br/>

<openmrs:extensionPoint pointId="org.openmrs.help" type="html" />


<%@ include file="/WEB-INF/view/module/legacyui/template/footer.jsp" %>